mkdir -p files && cd files
oc patch deployment my-useful-app --patch ' {"status":[], "metadata":{"managedFields":[]}}' --dry-run=client -o yaml > usefull-app-dep.yml
oc patch service my-useful-app --patch ' {"status":[], "metadata":{"managedFields":[]}}' --dry-run=client -o yaml > usefull-app-service.yml
oc patch route my-useful-app --patch ' {"status":[], "metadata":{"managedFields":[]}}' --dry-run=client -o yaml > usefull-app-route.yml
ls -al
cd ..