oc apply --dry-run=client -f usefulapp-dep.yml
oc apply --dry-run=client -f usefulapp-route.yml
oc apply --dry-run=client -f usefulapp-service.yml
